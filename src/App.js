import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import movielist from './movielist.json';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Detail from './Detail';


function App() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    async function getData() {
      const response = await fetch('/api/movies');
      const payload = await response.json();
      setMovies(payload.data);
    }
    getData();
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <p>Nice Movies:</p>
        <Router>
        <Route path="/"  exact component={Home}/>
        <Route path="/:id"  component={Detail}/>  
        </Router>
      </header>
    </div>
  );
}


function Home(){
  return (
    <div>
      {movielist.map(movie => (
        <div key={movie.id} >
          <Link to={`/${movie.id}`}>
            <h2>{ movie.title }</h2>
            <p> { movie.tagline }</p>
             <p> { movie.vote_average}</p>
          </Link>
        </div>
                ))}
      </div>
  );
}

export default App;
